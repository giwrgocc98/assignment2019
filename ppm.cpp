#include "ppm/ppm.h"
#include <string>
#include <fstream> // used for file reading 
#include <iostream>	

using namespace std; //bad practice

namespace image {

	float* ReadPPM(const char* filename, int* w, int* h) {

		ifstream file; //file name
		string headerPPM;
		int maxColorPPM = 0; //header and color ( must be less than 255)
		file.open(filename, ios::binary); //open file

		if (file.fail()) {	// checks whether bits are faulty
			cout << "Error while trying to open file" << endl;
			file.close();
			return nullptr;
		}


		file >> headerPPM; //reads first line which is the header type (must be P6)

		if (headerPPM == "p6" || headerPPM == "P6") {
			file >> *(w) >> *(h) >> maxColorPPM; //reads line by line the width , height , and color (0-255)

			if (file.fail()) {
				cout << "Error with the P6 header" << endl;
				file.close();
				return nullptr;
			}

		}

		if (*(w) < 0 || *(h) < 0 || (maxColorPPM < 0 || maxColorPPM > 255)) {
			cout << "Dimensions or color bytes are out of bounds";
			file.close();
			return nullptr;
		}

		cout << "Image dimensions are: " << *w << " X " << *h << endl;
		

		file.ignore(1, '\n'); //skipping stupid ass lines
	
		int imageSize = 3 * (*(w)) * (*(h)); // size = 3*w*h
		
		float* bufferPtr = new float[imageSize]; // pointer of newly allocated array of floats
		
		char* bufferReader = new char[imageSize]; // char array for reading of pixels

		file.read(bufferReader, imageSize); //reads as many characters as the size of the image

		unsigned char* bufferReaderUnsigned = (unsigned char*) bufferReader; // cast to unsigned 

		for (int i = 0; i < imageSize; i++) {
			*(bufferPtr + i) = (bufferReaderUnsigned[i]) / 255.0f;
		}


		return bufferPtr;
	}


	bool WritePPM(const float* data, int w, int h, const char* filename)
	{
		cout << "Writing file";
		ofstream file;
		file.open(filename, ios::binary | ios::out);
		if (file.fail()) {
			cerr << "Error while trying to write file \n";
			return false;
		}
		else {
			file << "P6 \n" << w << "\n" << h << "\n" << "255\n";

			int imageSize = 3 * h * w;
			char* bufferReader = new char[imageSize];
			for (int i = 0; i < imageSize; i++) {
				*(bufferReader + i) = (char)((*(data + i)) * 255.0f);
			}
			file.write(bufferReader, imageSize);
			file.close();

			delete[]bufferReader;
			return true;
		}
	}

}