#pragma once
#include "Image.h"
#include "vec3.h"
#include "Image.h"
#include <iostream>
#include "array2d.h"
#include "imageio.h"
#include <math.h>

typedef math::Vec3<float> Color;
using namespace image;
using namespace math;

class Filter {

	public:

		virtual Image operator << (const Image& image) = 0;

		Filter() {

		}

		~Filter() {

		}

};

class FilterLinear : public Filter , math::Array2D<Color> {

	protected:
		float a1, a2, a3, c1, c2, c3;

	public:
		Image operator << ( const Image& image) {


			Image new_img = image;
			               
			Color* a = new Color(a1, a2, a3); 
			Color* c = new Color(c1, c2, c3); 

			Color currentPixel;

			int w = new_img.getWidth();
			int h = new_img.getHeight();
			Color* ptr1 = new_img.getRawDataPtr(); //where our pre edited data are

			
			std::vector<Color> local_buffer;
			for (int i = 0; i < w*h; i++) {
				
				currentPixel = Color((*(ptr1 + i)).r ,(*(ptr1 + i)).g , (*(ptr1 + i)).b);
				
				currentPixel = ((currentPixel) * (*a) + (*c));
				currentPixel = currentPixel.clampToUpperBound(1);
				currentPixel = currentPixel.clampToLowerBound(0);
				local_buffer.push_back(currentPixel);

			}

			new_img.setData(local_buffer.data());
			return new_img;
		}

		FilterLinear(float a1,float a2,float a3,float c1,float c2,float c3) {
			this->a1 = a1;
			this->a2 = a2;
			this->a3 = a3;
			this->c1 = c1;
			this->c2 = c2;
			this->c3 = c3;

		}


};


class FilterGamma : public Filter {

	protected:
		float g;

	public:
		Image operator << (const Image& image) {
			Image new_img = image;


			Color currentPixel;

			int w = new_img.getWidth();
			int h = new_img.getHeight();
			Color* ptr1 = new_img.getRawDataPtr(); //where our pre edited data are


			std::vector<Color> local_buffer;
			for (int i = 0; i < w * h; i++) {

				
				currentPixel = Color(pow((*(ptr1 + i)).r,g), pow((*(ptr1 + i)).g,g), pow((*(ptr1 + i)).b,g));
				local_buffer.push_back(currentPixel);

			}

			new_img.setData(local_buffer.data());
			return new_img;



		}
		FilterGamma(double g) {
			this->g = g;
		}

};


class FilterBlur : public Filter {






};