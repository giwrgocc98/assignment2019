#pragma once
#include <string>
#include "imageio.h"
#include <iostream>
#include "ppm/ppm.h"
#include "array2d.hpp"
#include "array2d.h"
#include "vec3.h"


using namespace std;

typedef math::Vec3<float> Color; //recommended practice

class Image : image::ImageIO , math::Array2D<Color>{

	

	public:
		Image() :ImageIO(), math::Array2D<Color>::Array2D() {}

		bool image::ImageIO::load(const string& filename, const string& format) {

			if (format != "ppm") { //if the file is not ending in ppm 
				cerr << "Not a ppm image. \n"; //print error message
				return false;
			}

			int* width_pointer = new int[1]; //width pointer
			int* height_pointer = new int[1]; //height pointer 
			const char* filename_pointer = filename.c_str(); //filename pointer converted from string to char

			float* data = image::ReadPPM(filename_pointer, width_pointer, height_pointer); //returns in data a float pointer
																				//of the new array with all the input data 

			if (data == nullptr) {

				delete[] width_pointer; //deallocate heap memory
				delete[] height_pointer; //by deleting the pointers
				return false;
			}

			else {
				width = *(width_pointer);
				height = *(height_pointer);

				buffer.clear();
				for (unsigned int i = 0; i < width * height * 3; i = i + 3) { //DO NOT LOSE THAT PIXEL AGAIN	
					buffer.push_back(math::Vec3<float>(*(data + i), *(data + i + 1), *(data + i + 2)));
				}
				cout << buffer.size();
				return true;
			}

		}

		bool image::ImageIO::save(const string& filename, const string& format) {

			if (buffer.empty()) { //no data in the buffer
				cout << "Buffer is empty";
				return false;

			}

			const char* filename_pointer = filename.c_str(); //pointer of filename
			if (format == "ppm") {
				return image::WritePPM((float*)buffer.data(), width, height, filename_pointer);
			}
			else {
				return false;
			}
		}

		const unsigned int getWidth() const {
			return width;
		}

		const unsigned int getHeight() const {
			return height;
		}
		
		Color* getRawDataPtr()  {
			return (Color*) buffer.data();
		}

		void setData(const Color* const& data_ptr) {
			buffer.clear();
			for (int i = 0; i < width * height; i++) {
				Color current = *(data_ptr + i);
				buffer.push_back(current);
			}
		}

		Image(unsigned w, unsigned h, Color* ptr) {
			Array2D(w,h, ptr);
		}

		
};